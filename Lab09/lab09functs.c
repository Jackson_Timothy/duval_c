//
// Created by C22Timothy.Jackson on 9/5/2019.
//
/** lab09.c
* ===========================================================
* Name: Timothy Jackson, Sept 5, 2019
* Section: T3-T4
* Project: Multiple Files, Debugging, And Testing
* Purpose: Understand the general idea behind preprocessor directives.
Know the difference between #include and #include “geometry.h”.
Understand Unit Testing and the terms test bench (test harness).
Understand how to use the assert statement to perform unit testing.
Understand how to use multiple c-files in and be able to configure CLion/Cmake accordingly.
Understand the purpose and use of header file guards
Know the concept of Test Driven Development
* ===========================================================
*/

#include <stdio.h>
#include <math.h>

double volumeCylinder(double rad, double height){
    double vol;

    vol = M_PI * pow(rad,2) * height;

    return vol;
}

double volumeBox(double wid, double hi, double depth) {
    double volu;

    volu = wid * hi * depth;

    return volu;
}
double degToRad(double deg){
    double raddd;

    raddd = deg * (M_PI/180);

    return raddd;
}



