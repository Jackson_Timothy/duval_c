//
// Created by C22Timothy.Jackson on 9/5/2019.
//
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#ifndef MYEXE_LAB09FUNCTS_H
#define MYEXE_LAB09FUNCTS_H

double volumeCylinder(double rad, double height);
double volumeBox(double wid, double hi, double dep);
double degToRad(double deg);

#endif //MYEXE_LAB09FUNCTS_H
