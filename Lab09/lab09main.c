//
// Created by C22Timothy.Jackson on 9/5/2019.
//
/** lab09.c
* ===========================================================
* Name: Timothy Jackson, Sept 5, 2019
* Section: T3-T4
* Project: Multiple Files, Debugging, And Testing
* Purpose: Understand the general idea behind preprocessor directives.
Know the difference between #include and #include “geometry.h”.
Understand Unit Testing and the terms test bench (test harness).
Understand how to use the assert statement to perform unit testing.
Understand how to use multiple c-files in and be able to configure CLion/Cmake accordingly.
Understand the purpose and use of header file guards
Know the concept of Test Driven Development
* ===========================================================
*/

#include <stdio.h>
#include <math.h>
#include "lab09functs.h"

int main(){

    double rad;
    double height;
    double wid;
    double hi;
    double dep;
    double deg;

    printf("Enter cylinder radius followed by height:\n");
    scanf("%lf %lf",&rad,&height);
    printf("The volume is %lf\n", volumeCylinder(rad,height));

    printf("\nEnter box width followed by height followed by depth:\n");
    scanf("%lf %lf %lf", &wid, &hi, &dep);
    printf("The volume is %lf\n", volumeBox(wid,hi,dep));

    printf("\nEnter degrees:\n");
    scanf("%lf",&deg);
    printf("%lf degrees is %lf radians.\n", deg, degToRad(deg));

    return 0;
}
