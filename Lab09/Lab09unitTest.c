//
// Created by C22Timothy.Jackson on 9/5/2019.
//
#include <assert.h>
#include "lab09functs.c"

int main(){

    printf("Testing started\n");

    assert(volumeCylinder(0,0) == 0);


    assert(volumeBox(0,0,0) == 0);


    assert(degToRad(0) == 0);


    printf("Testing complete");
    return 0;
}